// Browser Check for canSwap and changeText
var canSwap = document.images ? true : false ;
var changeText = document.getElementById ? true : false ;


// swapImage function
// choose imageName 
function swapImage(imgId, imgSrc) {
	if (canSwap) {
		document.images[imgId].src = imgPath + imgSrc;
	}
}

// loadImages 
var loadList = new Array();
var imageList = new Array('image_map_m.gif', 'image_map_a.gif', 'image_map_b.gif', 'image_map_v.gif');

// loadImages function
window.onload = function() {
		for(var i = 0; i < imageList.length; i++) {
			loadList[i] = new Image();
			loadList[i].src = imgPath + imageList[i];
		}
//load Icons!!
		function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
		}
	var icons = {
			'icon-envelope' : '&#x65;',
			'icon-menu' : '&#x6d;',
			'icon-facebook' : '&#x66;',
			'icon-twitter' : '&#x74;',
			'icon-cart' : '&#x63;',
			'icon-magnifying-glass' : '&#x73;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};


//Change Title and Subtitle
var newSubtitle = new Array();


function changeTitle(number) {
	if (changeText) {
			document.getElementById("subtitle").innerHTML = newSubtitle[number];
	}
}


newSubtitle[0] = " <h4>Choose Whisky by Brand or <br />View All the bottles in the Collection</h4><br />";

newSubtitle[1] = "<h4>Macallan Private Collection<br />View all Macallan Whiskies</h4><br />";

newSubtitle[2] = "<h4>Ardbeg Private Collection<br />View all Ardbeg Whiskies</h4><br />";

newSubtitle[3] = "<h4>View Bowmore Private Collection<br />View all Bowmore Whiskies</h4><br />";

newSubtitle[4] = "<h4>View All the Bottles and Other Brands <br />in the Exclusive Collection</h4><br />";



