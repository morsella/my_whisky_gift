<script type="text/javascript">
	// <![CDATA[ //
	$(document).ready(function(){
			var tmb1 = $('.bottleImage ul li #tmb1'),
				tmb2 = $('.bottleImage ul li #tmb2'),
				tmb3 = $('.bottleImage ul li #tmb3'),
				tmb4 = $('.bottleImage ul li #tmb4');
			var bottle1 = $( ".bottleImage #productBottle" ),
			    bottle2 = $( ".bottleImage #productBottle2" ),
			    bottle3 =  $( ".bottleImage #productBottle3" ),
			    bottle4 = $( ".bottleImage #productBottle4" );	
			
			var	currentImg = bottle1;    
		
		tmb1.click(function(){
			currentImg = bottle1;
			currentImg.fadeTo("slow", 1);
			tmb1.addClass('active_tmb');
			tmb2.removeClass('active_tmb');
			tmb3.removeClass('active_tmb');
			tmb4.removeClass('active_tmb');
			bottle2.fadeTo("slow", 0);
			bottle3.fadeTo("slow", 0);
			bottle4.fadeTo("slow", 0);
		})
		tmb2.click(function(){
			currentImg = bottle2;
			currentImg.fadeTo("slow", 1);
			tmb2.addClass('active_tmb');
			tmb3.removeClass('active_tmb');
			tmb4.removeClass('active_tmb');
			tmb1.removeClass('active_tmb');
			bottle1.fadeTo("slow", 0);
			bottle3.fadeTo("slow", 0);
			bottle4.fadeTo("slow", 0);
		})
		tmb3.click(function(){
			currentImg = bottle3;
			currentImg.fadeTo("slow", 1);
			tmb3.addClass('active_tmb');
			tmb4.removeClass('active_tmb');
			tmb1.removeClass('active_tmb');
			tmb2.removeClass('active_tmb');
			bottle4.fadeTo("slow", 0);
			bottle1.fadeTo("slow", 0);
			bottle2.fadeTo("slow", 0);
		})
		tmb4.click(function(){
			currentImg = bottle4;
			currentImg.fadeTo("slow", 1);
			tmb4.addClass('active_tmb');
			tmb1.removeClass('active_tmb');
			tmb2.removeClass('active_tmb');
			tmb3.removeClass('active_tmb');
			bottle1.fadeTo("slow", 0);
			bottle2.fadeTo("slow", 0);
			bottle3.fadeTo("slow", 0);
		})
});

	// ]]> 	
</script>
	