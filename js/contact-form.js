function validateForm(){
		 var correct = true, 
		     error = "",
		     firstName = document.getElementById("firstSender").value,
		     lastName = document.getElementById("lastSender").value,
		     email = document.getElementById("emailSender").value,
		     att = email.indexOf("@"), 
		     dot = email.indexOf(".", att),
		     age = document.getElementById("verifyAge"),
		     textField = document.getElementById("senderText").value;
		     
		     if(firstName.length<2){
			     error += "&#8727; You must fill in your First name <br /> ";
			     correct = false;
		     }
		     if(lastName.length<2){
			     error += "&#8727; You must fill in your Last name <br /> ";
			     correct = false;
		     }
		     if(dot <= att+2 || email.length <= dot+2){
			     error += "&#8727; You must fill in a valid email address <br />  ";
			     correct = false;
		     }
		     if(textField<2){
			     error += "&#8727; Write your message <br />  ";
			     correct = false;
		     }
		     if(age.checked == false){
			     error += "&#8727; You must verify your age <br />  ";
			     correct = false;
		     }
		     
		     if(correct){
			     return true;
		     }else{
			     document.getElementById("error_message").innerHTML="<p>" + error + "</p>";
			     return false;
		     }
	}



