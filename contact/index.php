<?php

	require_once("../inc/config.php");

if($_SERVER["REQUEST_METHOD"] == "POST"){ 
$first_sender = trim($_POST["firstSender"]);
$last_sender = trim($_POST["lastSender"]);
$email_sender = trim($_POST["emailSender"]);
$text_sender = trim($_POST["senderText"]);

if($first_sender == "" OR $email_sender == "" OR $last_sender == "" OR $text_sender == ""){
	$error_message =  " You must fill in all the fields ";
}
if(!isset($error_message) && !isset($_POST["verifyAge"])){
	$error_message= " You must verify your age ";
}
if(!isset($error_message)){
		foreach($_POST as $value){
		if(stripos($value, 'Content-Type:') !== FALSE){
			$error_message =" There was a problem with the information you've entered. ";
		}
	}
}


if(!isset($error_message) && $_POST["sender_address"] != ""){
	$error_message= " Your form submission has an error ";
}
if($_POST["sender_country"] != ""){
	$error_message= " Your form submission has an error ";
}

require_once(ROOT_PATH."inc/phpmailer/PHPMailerAutoload.php");
require_once(ROOT_PATH."inc/phpmailer/class.phpmailer.php");
require_once(ROOT_PATH."inc/phpmailer/class.smtp.php");
$mail = new PHPMailer();

if (!isset($error_message) && !$mail->ValidateAddress($email_sender)){
   $error_message= "You must specify a valid email address. ";
}
if (!isset($error_message)){
$email_body = "";
$email_body = $email_body. "First Name: ".$first_sender. "<br />";
$email_body = $email_body. "Last Name: " .$last_sender. "<br />";
$email_body = $email_body. "Message: " .$text_sender. "<br />";

$mail->IsSMTP();
$mail->SMTPSecure = 'ssl';
$mail->SMTPAuth = true;
$mail->Host = "hs6.name.com";
$mail->Port = 465;
$mail->Username = "admin@mywhiskygift.com";
$mail->Password = "LishtotBey@h@d";
$mail->SetFrom($email_sender, $first_sender);
$mail->Subject = "Contact My Whisky Gift | " .$first_sender . " " .$last_sender;
$mail->MsgHTML($email_body);
$address="admin@mywhiskygift.com";
$mail->AddAddress($address, $first_sender, $last_sender);

    if($mail->Send()) {
	    header("Location:" . BASE_URL. "contact/thanks/");
	    exit;
	    }else{
	      $error_message= " There was a problem sending the email: " . $mail->ErrorInfo;
	    }
    }
}

?>
<?php 
$pageTitle = "My Whisky Gift &#124; Contact";
$selectedPage = "contact";
$meta = "Contact My Whisky Gift by Filling up the Contact Form, My Whisky Gift responds Shortly";
include(ROOT_PATH."inc/header.php"); ?>
<section class="container">
<?php if(isset($_GET["status"]) && $_GET ["status"] == "thanks"){ ?>
	<section class="container">
		<div class="grid_12 thanks">
			<h1>Contact My Whisky Gift</h1>
			<h4>Thank you for your email! We&rsquo;ll be in touch shortly.</h4><br />
			<p>You will be redirected to My Whisky Gift Home Page in 5 sec.</p><br />
		</div>
		<br />
	</section>
<?php }else{ ?>
<h1>Contact My Whisky Gift</h1>
<fieldset class="grid_12 contact">
	<legend>Contact Form</legend>
	<form method="post" action="<?php echo BASE_URL; ?>contact/" enctype="application/x-www-form-urlencoded" onsubmit="return validateForm();" onreset="resetForm();" >
		<table>
			<tr class="grid_10">
				<td colspan="2"><h6>All Fields are required</h6></td>
			</tr>
			<tr class="grid_10">
				<td class="error_message"><p><?php 
					if(isset($error_message)){
						echo ' '.$error_message .' ';
					}
				?></p>
				</td>
				<td class="error_message" id="error_message">
				</td>
			</tr>

			<tr class="grid_10">
				<td class="grid_3 alpha">
					<label for="firstSender">First Name: </label>
				</td>
				<td class="grid_7 omega">
					<input type="text" size="29" id="firstSender" name="firstSender" value="<?php if (isset($first_sender)){echo htmlspecialchars($first_sender);} ?>" />
				</td>
			</tr>
			<tr class="grid_10">
				<td class="grid_3 alpha">
					<label for="lastSender">Last Name: </label>
				</td>
				<td class="grid_7 omega">
					<input type="text" size="29" id="lastSender" name="lastSender" value="<?php if (isset($last_sender)){echo htmlspecialchars($last_sender);} ?>" />
				</td>
			</tr>
			<tr class="grid_10">
				<td class="grid_3 alpha">
					<label for="emailSender">Email:</label>
				</td>
				<td class="grid_7 omega">
					<input type="text" size="29" id="emailSender" name="emailSender" value="<?php if (isset($email_sender)){echo htmlspecialchars($email_sender);} ?>" />
				</td>
			</tr>	
			<tr class="grid_10" style="display:none;">
				<td class="grid_3"><label for="sender_address">Address:</label></td>
				<td><input type="text" name="sender_address" id="sender_address" />
					<p>Please leave this field blank for security reasons.</p>
				</td>
			</tr>	
			<tr class="grid_10" style="display:none;">
				<td class="grid_3"><label for="sender_country">Country:</label></td>
				<td><input type="text" name="sender_country" id="sender_country" />
					<p>Please leave this field blank for security reasons.</p>
				</td>
			</tr>
			<tr class="grid_10">
				<td class="grid_3 alpha">
					<label for="senderText">Message:</label>
				</td>
				<td class="grid_7 omega">
					<textarea cols="30" rows="4" id="senderText" name="senderText"><?php if (isset($text_sender)){echo htmlspecialchars($text_sender);} ?></textarea>
				</td>
			</tr>
			<tr class="grid_10">
				<td  colspan="2">
					<div class="checkbox grid_10">
						<input type="checkbox" value="age" id="verifyAge" name="verifyAge" />
						<label for="verifyAge"></label>
						<span>I verify that I am at least 18 years old and at a legal age to consume alcohol in my country.	</span>
					</div>				
				</td>
			</tr>	
			<tr class="grid_10">
				<td class="grid_9"><input type="submit" value="Send" id="submit" /></td>
				<td class="grid_1"><input type="reset" value="Reset" onclick="return confirm('You are about to reset all the fields')" id="reset" /></td>
			</tr>
		</table>
	</form>
</fieldset>
	<?php } ?>
</section>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/contact-form.js"></script>

<?php include(ROOT_PATH."inc/footer.php"); ?>