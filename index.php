<?php 
	
	require_once("inc/config.php");

$pageTitle = "My Whisky Gift";
$selectedPage = "home";
include(ROOT_PATH."inc/header.php"); 
include(ROOT_PATH."inc/products.php");
$recent = get_products_recent();
$myCurrentElement = end($recent);
$myLastElement = prev($recent); 
$myPrevElement = prev($recent);
?>

			<!-- _____________SLIDER___________________________________ -->
		
	<section>
				 <!-- BEGIN .slider -->
	    <div class="slider">
	        <div class="sliderWrapper">
	            <article class="whiskyIntro">
	            	<div>
	                <h1>Welcome to My Whisky Gift</h1>
					<h3>The Best gift for anyone
						<br /> who loves Whisky</h3>
	            	</div>
	            	
	            	<div class="large pointer-events"><img src="<?php echo BASE_URL; ?>imgs/bottle1.png" alt="bottle1" /></div>
	            	<div class="medium pointer-events"><img src="<?php echo BASE_URL; ?>imgs/bottle1_m.png" alt="bottle1_m" /></div>
	            	<div class="small pointer-events"><img src="<?php echo BASE_URL; ?>imgs/bottle1_s.png" alt="bottle1_s" /></div>
	            	<div class="hd pointer-events"><img src="<?php echo BASE_URL; ?>imgs/bottle1_xsx2.png" alt="bottle1_xsx2" /></div>
	            	
	            </article>
	    
	            <article class="specialSale">
				   <div>
	                <h1>The Exclusive <br />Collection Page</h1>
					<h3>Lets you see the list of the
						<br />Rare Whisky Collectables </h3>
	            	</div>
	            	
	            	<div class="large pointer-events"><img src="<?php echo BASE_URL; ?>imgs/collection.gif" alt="bottle1" /></div>
	            	<div class="medium pointer-events"><img src="<?php echo BASE_URL; ?>imgs/collection_m.gif" alt="bottle1_m" /></div>
	            	<div class="small pointer-events"><img src="<?php echo BASE_URL; ?>imgs/collection_s.gif" alt="bottle1_s" /></div>
	            	<div class="hd pointer-events"><img src="<?php echo BASE_URL; ?>imgs/collection.gif" alt="bottle1_xsx2" /></div>
	
	            	
	               	<div class="view_collection">
	        			       	<a href="<?php echo BASE_URL; ?>collection/" title="My Whisky Gift Exclusive Collection">View Collection</a>
					</div>
	           </article>    
	           <article class="specialSale2">
				   <div>
	                <h1>The Search Page</h1>
					<h3>Find the perfect Whisky<br />
					You can search by Brand,<br />
						Name, Year or Age </h3>	
	            	</div>
	            	
	            	<div class="large pointer-events"><img src="<?php echo BASE_URL; ?>imgs/search_intro.png" alt="bottle1" /></div>
	            	<div class="medium pointer-events"><img src="<?php echo BASE_URL; ?>imgs/search_intro_m.png" alt="bottle1_m" /></div>
	            	<div class="small pointer-events"><img src="<?php echo BASE_URL; ?>imgs/search_intro_s.png" alt="bottle1_s" /></div>
	            	<div class="hd pointer-events"><img src="<?php echo BASE_URL; ?>imgs/search_intro.png" alt="bottle1_xsx2" /></div>
	               	<div class="view_collection">
	        			       	<a href="<?php echo BASE_URL; ?>search/" title="My Whisky Gift Search Collection">Search Whisky</a>
					</div>          
				 </article>    
		</div>		  
    </div>
     <!-- END Slider -->
	</section>
	<div class="mov_bg">
 <!-- Begin Video -->
 		 <div id="video-container">
		  <!-- Video -->
		  <video id="video" controls preload poster="<?php echo BASE_URL ?>vid/poster.gif"  data-autoresize='fill'>
		    <source  src="<?php echo BASE_URL ?>vid/mywhiskygift.webm" type="video/webm" />
		    <source  src="<?php echo BASE_URL ?>vid/mywhiskygift.ogv" type="video/ogg" />
		    <source  src="<?php echo BASE_URL ?>vid/mywhiskygift.mp4" type="video/mp4" />
		     <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
		      <object id="flash_fallback_1" class="vjs-flash-fallback" type="application/x-shockwave-flash" 
		        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
		        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
		        <param name="allowfullscreen" value="true" />
		        <param name="flashvars" 
		          value='config={"playlist":["<?php echo BASE_URL; ?>vid/poster.mov", {"url": "<?php echo BASE_URL; ?>vid/mywhiskygift.mp4","autoPlay":false,"autoBuffering":true}]}' />
		        <!-- Image Fallback. Typically the same as the poster image. -->
		        <img src="<?php echo BASE_URL; ?>vid/poster.gif" alt="My Whisky Gift Poster Image" 
		          title="No video playback capabilities." />
		      </object>
		   </video>   
		  </div>
	    <div id="video-container-m">    
		   <video id="video-m" controls preload poster="<?php echo BASE_URL ?>vid/poster.gif">
		    <source  src="<?php echo BASE_URL ?>vid/mywhiskygift_m.webm" type="video/webm" />
		    <source  src="<?php echo BASE_URL ?>vid/mywhiskygift_m.ogv" type="video/ogg" />
		    <source  src="<?php echo BASE_URL ?>vid/mywhiskygift_m.mp4" type="video/mp4" />
		     <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
		      <object id="flash_fallback_2" class="vjs-flash-fallback" type="application/x-shockwave-flash" 
		        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
		        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
		        <param name="allowfullscreen" value="true" />
		        <param name="flashvars" 
		          value='config={"playlist":["<?php echo BASE_URL; ?>vid/poster.mov", {"url": "<?php echo BASE_URL; ?>vid/mywhiskygift_m.mp4","autoPlay":false,"autoBuffering":true}]}' />
		        <!-- Image Fallback. Typically the same as the poster image. -->
		        <img src="<?php echo BASE_URL; ?>vid/poster.gif" alt="My Whisky Gift Poster Image" 
		          title="No video playback capabilities." />
		      </object>
		    </video> 
		  </div>
	   <div id="video-container-s">    
  		  <video id="video-s" controls preload poster="<?php echo BASE_URL ?>vid/poster.gif">
		    <source src="<?php echo BASE_URL ?>vid/mywhiskygift_s.webm" type="video/webm" />
		    <source src="<?php echo BASE_URL ?>vid/mywhiskygift_s.ogv" type="video/ogg" />
		    <source src="<?php echo BASE_URL ?>vid/mywhiskygift_s.mp4" type="video/mp4" />
		     <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
		      <object id="flash_fallback_3" class="vjs-flash-fallback" type="application/x-shockwave-flash" 
		        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
		        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
		        <param name="allowfullscreen" value="true" />
		        <param name="flashvars" 
		          value='config={"playlist":["<?php echo BASE_URL; ?>vid/poster.mov", {"url": "<?php echo BASE_URL; ?>vid/mywhiskygift_s.mp4","autoPlay":false,"autoBuffering":true}]}' />
		        <!-- Image Fallback. Typically the same as the poster image. -->
		        <img src="<?php echo BASE_URL; ?>vid/poster.gif" alt="My Whisky Gift Poster Image" 
		          title="No video playback capabilities." />
		      </object>
		  </video>

		  <!--Custom Video Controls -->
<!--
		  <div id="video-controls">
		    <button type="button" id="play-pause">Play</button>
		    <input type="range" id="seek-bar" value="0">
		    <button type="button" id="mute">Mute</button>
		    <input type="range" id="volume-bar" min="0" max="1" step="0.1" value="1">
		    <button type="button" id="full-screen">Full-Screen</button>
		 </div>
-->
			</div>
		</div>
  <!-- End Video -->	
	<section class="container clearfix">
			<div class="grid_12 devider pointer-events">
					<img src="<?php echo BASE_URL; ?>imgs/devider.gif" alt="Whisky bottles devider" />
			</div>
			<div class="grid_12" id="whisky_bg">
				<?php 
				$position=0;
				$total_bottles=count($recent);
				$list_view_html = "";

				foreach($recent as $product){ 
					$position +=1;
					if($total_bottles - $position <1){
						$list_view_html = get_newest($product).$list_view_html;
					}
				} 
				echo $list_view_html;
				?>
				<?php 
				$total_bottles = count($recent);
				$position = 0;
				$list_view_html = "";
				
				foreach($recent as $product){ 
					$position +=1;
					if($total_bottles - $position >= $total_bottles-1){
						$list_view_html = get_latest($product).$list_view_html;
					}
				} 
				echo $list_view_html;
				?>
				
				<div class="grid_12 pointer-events"><img src="<?php echo BASE_URL; ?>imgs/devider_1.gif" alt="Whisky bottles devider" /></div>
			</div>
	</section>
	<section class="container flashContent">
		<div id="flashContent" class="grid_12">
			<object  id="fade-in" data="fade-in.swf" type="application/x-shockwave-flash">
				<param name="movie" value="fade-in.swf" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#f9f9f9" />
				<param name="play" value="true" />
				<param name="loop" value="true" />
				<param name="wmode" value="window" />
				<param name="scale" value="showall" />
				<param name="menu" value="false" />
				<param name="devicefont" value="false" />
				<param name="salign" value="" />
				<param name="allowScriptAccess" value="sameDomain" />
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="fade-in.swf" width="960" height="466">
					<param name="movie" value="fade-in.swf" />
					<param name="quality" value="high" />
					<param name="bgcolor" value="#f9f9f9" />
					<param name="play" value="true" />
					<param name="loop" value="true" />
					<param name="wmode" value="window" />
					<param name="scale" value="showall" />
					<param name="menu" value="false" />
					<param name="devicefont" value="false" />
					<param name="salign" value="" />
					<param name="allowScriptAccess" value="sameDomain" />
				<!--<![endif]-->

				<div id="photoShow" class="grid_12">
						<div class="current">
							<img src="<?php echo BASE_URL; ?>imgs/ardbeg.jpg" alt="My Whisky Gift Photo Gallery Ardbeg" class="grid_12 pointer-events" /> 
						</div>
						<div>
							<img src="<?php echo BASE_URL; ?>imgs/mcallan.jpg" alt="My Whisky Gift Photo Gallery Macallan" class="grid_12 pointer-events" /> 
						</div>
						<div>
							<img src="<?php echo BASE_URL; ?>imgs/mcallan2.jpg" alt="My Whisky Gift Photo Gallery Macallan" class="grid_12 pointer-events" /> 
						</div>
				</div>
	
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
		</div>

	</section>
		<script type="text/javascript">
    // <![CDATA[
    	
    	$("document").ready( function(){
    		
    		setInterval( "rotateImages()", 2000 );
    		
    	});
    	
    	function rotateImages(){
    		
    		var curFoto = $( "#photoShow div.current" );
    		var nextFoto = curFoto.next();
    		
    		if( nextFoto.length == 0 ){
    			nextFoto = $( "#photoShow div:first" );
    		}
    		
    		curFoto.removeClass( "current" ).addClass("previous");
    		nextFoto.css( "opacity", 0.0 ).addClass( "current" ).animate( { opacity:1.0 }, 500, function(){
    		
    			curFoto.removeClass( "previous" );
    		
    		});
    		
    		
    	};
    		
    // ]]>
    </script> 
<?php include(ROOT_PATH."inc/footer.php"); ?>	
