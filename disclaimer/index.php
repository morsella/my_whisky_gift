<?php

	require_once("../inc/config.php");

$pageTitle = "My Whisky Gift &#124; Disclaimer";
$selectedPage = "disclaimer";
$meta = "My Whisky Gift Disclaimer and Privacy Policy To ensure safe and secure browsing and shopping experience";
include(ROOT_PATH."inc/header.php");
include(ROOT_PATH."inc/products.php");
?>
<section class="container">
	<div class="grid_12 disclaimer">
				<h1>My Whisky Gift Disclaimer and Terms</h1>
				<h4>General Terms and Conditions of Sale</h4><br />
				<h5>Payment</h5>
				<p>We accept Paypal and various Credit Cards through Paypal. We do not charge for any item until it is ready to ship. Backordered items are not charged until they are shipped. You may also pay through bank transfer but please contact us for details first.</p>
				<p>When confirmation of order is received, this is to indicate that we have received your order. It does not indicate that a contract exists between us. We will indicate acceptance of your order, and hence a contract between us, when we send you an invoice. We have included this term to protect us in the case that a mistake has been made in pricing, we have inadvertently under-priced goods, or we are no longer able to supply a particular product for some reason. In the case of a change of price, we will always contact you first to ensure that the price is acceptable.</p>
				<p>You must confirm that you are of legal age to purchase alcohol within your country of residence &#40; UK 18, USA 21 &#41;.</p>
				<p>If the order is being delivered to a different address, the recipient must also be of legal age to receive alcohol. This is the responsibility of the purchaser.</p>
				<h5>Security</h5>
				<p>All personal information including your name, address, phone number, email address, login password etc is all entered on our secure server which uses SSL. This means the data is encrypted before it is sent securely across the Internet. All credit and debit card numbers are entered solely on the secure server of WorldPay systems, and the communication between WorldPay and the worldwide banking networks, is regularly audited by the banking authorities to ensure a secure transaction environment. They also ensure that they stay up-to-date with the latest versions of any third-party code they use, and continually review their own proprietary code.</p>
				<h5>Orders from Outside the European Union &#40;EU&#41;</h5>
				<p>All transactions are made in Euros. If you are ordering from outside the Eurozone the actual exchange rate used for the transaction is determined by your credit card company or bank who will convert the transaction to your own currency &#40; e.g. US Dollars, GBP etc&#41;. Please note: if we choose to display prices in other currencies on this website, these prices may vary slightly from the actual price you pay. This is because the exchange rate used by your credit card company or bank may vary slightly from the one we are using. We aim to keep all prices as accurate as possible and our exchange rate is updated regularly.</p>
				<h5>Back Orders</h5>
				<p>If your item is not in stock, we will back order for you. You will always be emailed with the option to cancel your order if you would rather not wait.</p>
				<h5>Tax Charges</h5>
				<p>Orders made from within the EU are subject to VAT. If we are sending products to an address within the European Union, prices are subject to VAT. Orders sent to an address outside the EU, are not charged VAT, although your order may be subject to local taxes and duties upon arrival in your country, which are levied once the package reaches your country. Any additional charges for customs clearance must be borne by you the customer or the recipient, we have no control over these charges and cannot predict the charges. Customs policies vary widely from country to country, so you will need to contact your local customs office if you require further information.</p>
				<h5>Guarantee</h5>
				<p>We guarantee your satisfaction. All items are, to the best of our knowledge, authentic and original. We strive to inform you as best we can in text and pictures of the condition of the item. We will never purposely withhold details that could have a negative influence to the value of the item. </p>
				<p>All of our products come with a 30 day no quibble guarantee. &#40;Excluding pre-arranged special orders&#41;. Goods must be returned within 30 days. The goods must be returned postage paid in the same condition in which they were received. We cannot refund purchases if the original packaging or seals have been broken or tampered with in any way.</p>
				<p>All goods remain the property of My Whisky Gift.com until payment is received in full.</p>
				<h5>Privacy Policy</h5>
				<p>MyWhisky Gift.com do not disclose buyers' information to third parties other than when order details are processed as part of the order fulfilment. In this case, the third party will not disclose any of the details to any other third party.
			Cookies are used on this shopping site to keep track of the contents of your shopping cart. They are also used after you have logged on as part of that process. You can turn off cookies within your browser by going to &#8242;Tools &#124; Internet Options &#124; Privacy&#8242; and selecting to block cookies. If you turn off cookies, you will be unable to place orders or benefit from the other features that use cookies. Data collected by this site is used to:</p>
				<p>a. Take and fulfil customer orders</p>
				<p>b. Administer and enhance the site and service</p>
				<p>c. Only disclose information to third-parties for goods delivery purposes</p>
				<h5>Returns Policy</h5>
				<p>It is MyWhiskyGift.coms policy to ensure that all products supplied to all our customers arrive in perfect condition. If you are unhappy with any purchase  which is not faulty, we will happily refund your money, &#40;excluding the delivery charge and any duties incurred in the return&#41;, or exchange the products if you inform us within 7 days. Any returned products must be returned to us in perfect condition, with all packaging intact. All refunds and reimbursements will be paid within 30 days of of the return of the products. Any product returned must be within its original packaging and with unbroken seals. The cost of returning the product is the responsibility of the customer.
				It is the customers responsibility to make sure the product is adequately packed to avoid any possible damage.</p>
				<p>Any and all returns must be addressed to:</p>
				<p>MyWhiskyGift.com<br />
					Noordeinde <br />
					St.Pancras<br />
					1834AE<br />
					Netherlands</p>

	</div>



</section>
<?php include(ROOT_PATH."inc/footer.php"); ?>