<?php 
	
	require_once("../inc/config.php");

$pageTitle = "My Whisky Gift &#124; About";
$selectedPage = "about";
$meta = "About My Whisky Gift Idea, the Exclusive Collection, and the Owner of My Whisky Gift Collection";
include(ROOT_PATH."inc/header.php"); 
include(ROOT_PATH."inc/products.php");
?>
<section class="container about">
	<h1>About the Collection</h1>
		<p>Before all else, please note that this website is only about Single Malt Scotch whisky, meaning that the liquor in any bottle here by law comes from a single distillery and was distilled, matured and bottled in Scotland. </p>
		<p>It is not hard to come up with a few good reasons as to why we started My Whisky Gift.com. First of all, whisky is an immensely popular drink the world over. Men and women of all legal ages are amazed at the diversity of flavors, colors and 'nose' that is available. All the more so in places like India and China where consumers enjoy spending their newfound money on items of quality and luxury. Everybody enjoys receiving and giving a gift, especially if thoughtfully chosen and therefore appreciated more. When a close friend turned 70, I agonized over what to give him. People of that age usually have already everything they need for their sports or hobbies and I felt this birthday required something special. In the end I settled on a bottle of The Macallan that was in my collection. The whisky inside was distilled in the year my friend was born and the year 1939 featured prominently on the label. I will hunt for a picture of that occasion and post it here. </p>
		<p>On the day of the party many people showed surprise that such an old bottle still survives and let me know that it made a very nice and original present.  
		So the general idea was born and the details matured slowly, just like a fine whisky does in its cask. We hope you appreciate the effort and if you decide to buy your gift here we are quite sure it will make the lucky recipient as happy as my friend was that on that October day in 2009!
		The collection on offer here originates from various sources and many years of attending auctions, creeping through cobwebby cellars and happy negotiations with fellow aficionados.</p>

</section>
<?php include(ROOT_PATH."inc/footer.php"); ?>	
