<?php 
	
	require_once("../inc/config.php");

$pageTitle = "My Whisky Gift &#124; Receipt";
$selectedPage = "receipt";
$meta = "My Whisky Gift Receipt verifying that the Transaction was Completed Successfully";
include(ROOT_PATH. "inc/header.php"); ?>
<section class="container">
	<div class="grid_12 receipt">
		<h1>Thank you for your payment.</h1>
		
		<h4> Your transaction has been completed, and a receipt for your purchase has been emailed to you. You may log into your account at <a href="www.paypal.com" title="PayPal account to check your transaction" > www.paypal.com  </a>to view details of this transaction.</h4><br />
		<p>You will be redirected to My Whisky Gift Home Page in 10 sec.</p><br />
	</div><br />
</section>
<?php include(ROOT_PATH. "inc/footer.php") ?>