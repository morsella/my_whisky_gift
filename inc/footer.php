
<footer class="container clearfix footer">
				<div class="grid_12 pointer-events">
					<img src="<?php echo BASE_URL; ?>imgs/devider.gif" alt="Whisky bottles devider" />
				</div>
				<div class="grid_6">
					<ul class="social">
						<li><a href="https://www.facebook.com/morsella"><div class="icon-facebook"></div></a></li>
						<li><a href="http://www.twitter.com"><div class="icon-twitter"></div></a></li>
						<li><a href="<?php echo BASE_URL; ?>contact/"><div class="icon-envelope"></div></a></li>
					</ul>
				</div>
				<div class="grid_6 terms">
						<p>CALL US</p><br /><p> &#43; 31 &#40;0&#41;68 1955 885 </p><br />
						<a class="<?php if($selectedPage == "disclaimer") {echo "active";} ?>" href="<?php echo BASE_URL; ?>disclaimer/">Disclaimer &#38; Privacy</a><br />
						<a class="<?php if($selectedPage == "shipping") {echo "active";} ?>" href="<?php echo BASE_URL; ?>shipping/">Shipping Policy</a><br />
						<p>&copy; <?php echo date('Y'); ?> My Whisky Gift</p>
				</div>
</footer>
			<div class="footer_bg">
			</div>
</div>

	<script type="text/javascript">
			// <![CDATA[ //		
		$('#slideLeft').click(function(){
			$(this).toggleClass('menu-active');
			$('.pushMenu').toggleClass('rightPush');
			$('.menu').toggleClass('openMenu');
			disableOther( 'slideLeft' );
		});
		function disableOther(button) {
				if( button !== 'slideLeft')  {
					$('#slideLeft').toggleClass('menu-disabled');
				};
			};
		$(window).resize(function(){
		if(window.innerWidth > 768) {
			$(".pushMenu").removeClass('rightPush');
			$(".menu").removeClass('openMenu');
					
			}
		});
		
		$('.slider').startSlider({
            touch: true,
            animation: 'slide',
            slideshowSpeed: 4000,
           
        }); 
		
		$('.read_more').mouseover(function() {
			$()
		});
		
		// ]]> 	
	</script>

</body>
</html>
