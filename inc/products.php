<?php

/*  get_list_view_html
#### output variable that contain html and php to display the list of products
#### Argument : string $product   the product
#### Returns: array  -  a list of the products
*/
function get_list_view_html($product){
	$output = "";
	$output	= $output. '<li class="grid_4">';
	$output = $output . '<a class="mouse-events" href="' . BASE_URL . 'collection/' . $product["sku"] . '/">';
	$output	= $output. '<img src="'.BASE_URL.$product["img"].'" alt="'. $product["brand"]. " ".$product["name"] .'" class="collectionList pointer-events" />';
	$output	= $output. "</a>";
	$output	= $output. '<div class="short_description" ><p>View Details</p></div>';
	$output = $output .'<div class="grid_12"><img src="'.BASE_URL.'imgs/devider_1.gif" alt="Whisky bottles devider" class="collectionListDevider pointer-events" /></div>';
	$output	= $output. "</li>";

	return $output;
}

/*  get_newest
#### preview variable that contain html and php to display the list of products
#### Argument : string $product   the product
#### Returns: array  -  the last 4 products that were added and display the newest one
*/

function get_newest($product){
	$preview = "";
	$preview = $preview. '<div class="grid_6 description last">';
	$preview = $preview . '<h2>'.$product["brand"]. "<br /> " .$product["name"].'</h2>';
	$preview = $preview	. '<h4>'.$product["age"]."<br /> " .$product["year"].'<br /> &#40;'.$product["size"].'cl, '. $product["alcohol"].'&#37;&#41; </h4>';
	$preview = $preview . '<p>'.$product["desc"].'</p><br /><div class="grid_3">';
	$preview = $preview . '<a class="read_more" href="' . BASE_URL . 'collection/' . $product["sku"] . '/">read<br />more</a></div><div class="grid_3 price">';
	$preview = $preview . '<h5>&#8364;'.$product["price"].'</h5><a class="collection_link" href="'.BASE_URL.'collection/" title="view an Exlusive Whisky Collection">view collection</a></div></div><div class="grid_6 product pointer-events">';
	$preview = $preview .'<img id="productNew" src="'.BASE_URL.$product["img"].'" alt="'. $product["brand"]." ". $product["name"] .'"/></div>';
	$preview = $preview .'<div class="grid_12 productDevider pointer-events"><img src="'.BASE_URL.'imgs/devider_1.gif" alt="Whisky bottles devider" /></div>';
	return $preview;
}

/*  get_latest
#### preview variable that contain html and php to display the list of products
#### Argument : string $product   the product
#### Returns: array  -  the last 4 products that were added and display the latest one
*/
function get_latest($product){
	$preview = "";
	$preview = $preview .'<div class="grid_6 product pointer-events"><img id="productLast" src="'.BASE_URL.$product["img"].'" alt="'. $product["brand"]." ". $product["name"] .'"/></div>';
	$preview = $preview. '<div class="grid_6 description last">';
	$preview = $preview . '<h2>'.$product["brand"]. "<br /> " .$product["name"].'</h2>';
	$preview = $preview	. '<h4>'.$product["age"]."<br /> " .$product["year"].'<br /> &#40;'.$product["size"].'cl, '. $product["alcohol"].'&#37;&#41; </h4>';
	$preview = $preview . '<p>'.$product["desc"].'</p><br /><div class="grid_3">';
	$preview = $preview . '<a class="read_more" href="'.BASE_URL .'collection/'.$product["sku"] .'/">read<br />more</a></div><div class="grid_3 price">';
	$preview = $preview . '<h5>&#8364;'.$product["price"].'</h5><a class="collection_link" href="'.BASE_URL.'collection/" title="view an Exlusive Whisky Collection">view collection</a></div></div>';

	return $preview;
}

/*  get_products_recent
#### all variable that contain the list of all the products and loops through all the products and sets the position for each one of them
#### Returns: array  -  the last 4 products that were added and display all of them
*/

function get_products_recent(){
    $recent = array();
    $all = get_products_all();

    $total_products = count($all);
    $position = 0;

    foreach($all as $product) {
        $position = $position + 1;
        if ($total_products - $position < 4) {
            $recent[] = $product;
        }
    }
    return $recent;
}
/*
#### Loops through all the products, looking for a search term in the product names
#### Argument : string $search   the search term
#### Returns: array  -  a list of the products that contain the search term in their names
*/
function get_products_search($search){
	$results = array();
	$all = get_products_all();

	foreach($all as $product){
		if (stripos($product["name"], $search) !== false OR stripos($product["brand"], $search) !== false OR stripos($product["year"], $search) !== false OR stripos($product["age"], $search) !== false){
		$results[] = $product;
		}
	}
	return $results;
}

/*  get_products_count
#### Counts all the products with the $total_products variable
#### Returns: number of total products
*/
function get_products_count(){
	$all = get_products_all();
	$total_products = count($all);

	return $total_products;
}

/*  get_products_subset
#### Loops through all the products and their position to set the number of product to display regarding the position Start and position End
#### Argument : string $positionStart and $positionEnd  - are the numbers that set of what is the firtst and the last product to display in current page
#### Returns: array  -  the amount of products that set for the current page
*/

function get_products_subset($positionStart, $positionEnd){
	$subset = array();
	$all = get_products_all();

	$position = 0;
	foreach($all as $product){
		$position += 1;
		if($position >= $positionStart && $position <= $positionEnd){
			 $subset[] = $product;
		 }
	}
	return $subset;
}

function get_products_all(){
		$products = array();
		$products[601030] = array(
			"brand"=>"Wolf Legend",
			"name" => "Bowmore",
			"age" => "30 Year Old",
			"year" => "",
			"alcohol" => 43,
			"size" => 75,
			"price" => "295.00",
			"img" => "imgs/bottles/ww/ww601030.gif",
			"img2" => "imgs/bottles/ww/ww601030_2.jpg",
			"img3" => "imgs/bottles/ww/ww601030_3.jpg",
			"img4" => "imgs/bottles/ww/ww601030_4.jpg",
			"desc" => "Premium quality blended scotch whisky from the Morrison Bowmore Distillers portfolio.",
			"paypal" => "RVWZFS5VSZN54",
			"instock" => 1,
			"tmb1" => "imgs/bottles/ww/ww601030_t1.jpg",
			"tmb2" => "imgs/bottles/ww/ww601030_t2.jpg",
			"tmb3" => "imgs/bottles/ww/ww601030_t3.jpg",
			"tmb4" => "imgs/bottles/ww/ww601030_t4.jpg"

			);
		$products[201025] = array(
		   "brand"=>"Ardbeg",
			"name" => "Lord of the Isles",
			"age" => "25 Year Old",
			"year" => "",
			"alcohol" => 46,
			"size" => 70,
			"price" => "1250.00",
			"img" => "imgs/bottles/ar/ar201025.gif",
			"desc" => "This whisky embodies the richness, depth and sweetness of all Ardbegs. Light gold in colour and powerful in character. A phenomenal, much-sought-after bottling of a really outstanding malt.",
			"paypal" => "",
			"instock" => 1,

			);
		$products[401012] = array(
			"brand"=>"Laphroaig",
			"name" => "HighGrove",
			"age" => "12 Year Old",
			"year" => "",
			"alcohol" => 46,
			"size" => 70,
			"price" => "195.00",
			"img" => "imgs/bottles/lr/lr401012.gif",
			"desc" => "A very special single cask Laphraoig, the official home of Prince Charles. The prince is a big fan of Laphroaig so we imagine he'll be very pleased with this. It's rather good.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[501025] = array(
			"brand"=>"Bunnahabhain",
			"name" => "Single Malt Scotch Whisky",
			"age" => "25 Year Old",
			"year" => "",
			"alcohol" => 43,
			"size" => 70,
			"price" => "275.00",
			"img" => "imgs/bottles/bh/bh501025.gif",
			"desc" => "A more sherried style, with the soft, creamy complexity and gentle, sophisticated palate and mouthfeel that Bunna's aficionados treasure. Handsomely presented, this is a delicious, contemplative dram. Picked up the top prize in its category at the World Whisky Awards 2010.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[202017] = array(
			"brand"=>"Ardbeg",
			"name" => "Single Malt Scotch Whisky",
			"age" => "17 Year Old",
			"year" => "",
			"alcohol" => 40,
			"size" => 70,
			"price" => "395.00",
			"img" => "imgs/bottles/ar/ar202017.gif",
			"desc" => "Increasingly rare, now discontinued whisky from Ardbeg. Incredibly rich and powerful, and all too easy to drink.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[101001] = array(
			"brand"=>"Test Bottle",
			"name" => "Test Bottle",
			"age" => "Just made",
			"year" => "distilled in 2013",
			"alcohol" => 15,
			"size" => 70,
			"price" => 00.01,
			"img" => "imgs/bottles/101001.gif",
			"desc" => "A test bottle for paypal account",
			"paypal" => "",
			"instock" => 1,
		);
		$products[101002] = array(
			"brand"=>"Wolf Legend",
			"name" => "Bowmore",
			"age" => "30 Year Old",
			"year" => "",
			"alcohol" => 43,
			"size" => 75,
			"price" => "295.00",
			"img" => "imgs/bottles/ww/ww601030.gif",
			"desc" => "Premium quality blended scotch whisky from the Morrison Bowmore Distillers portfolio.",
			"paypal" => "",
			"instock" => 1,

			);
		$products[101003] = array(
		   "brand"=>"Ardbeg",
			"name" => "Lord of the Isles",
			"age" => "25 Year Old",
			"year" => "",
			"alcohol" => 46,
			"size" => 70,
			"price" => "1250.00",
			"img" => "imgs/bottles/ar/ar201025.gif",
			"desc" => "This whisky embodies the richness, depth and sweetness of all Ardbegs. Light gold in colour and powerful in character. A phenomenal, much-sought-after bottling of a really outstanding malt.",
			"paypal" => "",
			"instock" => 1,

			);
		$products[101004] = array(
			"brand"=>"Laphroaig",
			"name" => "HighGrove",
			"age" => "12 Year Old",
			"year" => "",
			"alcohol" => 46,
			"size" => 70,
			"price" => "195.00",
			"img" => "imgs/bottles/lr/lr401012.gif",
			"desc" => "A very special single cask Laphraoig, the official home of Prince Charles. The prince is a big fan of Laphroaig so we imagine he'll be very pleased with this. It's rather good.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[101005] = array(
			"brand"=>"Bunnahabhain",
			"name" => "Single Malt Scotch Whisky",
			"age" => "25 Year Old",
			"year" => "",
			"alcohol" => 43,
			"size" => 70,
			"price" => "275.00",
			"img" => "imgs/bottles/bh/bh501025.gif",
			"desc" => "A more sherried style, with the soft, creamy complexity and gentle, sophisticated palate and mouthfeel that Bunna's aficionados treasure. Handsomely presented, this is a delicious, contemplative dram. Picked up the top prize in its category at the World Whisky Awards 2010.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[101006] = array(
			"brand"=>"Ardbeg",
			"name" => "Single Malt Scotch Whisky",
			"age" => "17 Year Old",
			"year" => "",
			"alcohol" => 40,
			"size" => 70,
			"price" => "395.00",
			"img" => "imgs/bottles/ar/ar202017.gif",
			"desc" => "Increasingly rare, now discontinued whisky from Ardbeg. Incredibly rich and powerful, and all too easy to drink.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[101007] = array(
			"brand"=>"Test Bottle",
			"name" => "Test Bottle",
			"age" => "Just made",
			"year" => "distilled in 2013",
			"alcohol" => 15,
			"size" => 70,
			"price" => 00.01,
			"img" => "imgs/bottles/101001.gif",
			"desc" => "A test bottle for paypal account",
			"paypal" => "",
			"instock" => 1,
		);
		$products[101008] = array(
			"brand"=>"Wolf Legend",
			"name" => "Bowmore",
			"age" => "30 Year Old",
			"year" => "",
			"alcohol" => 43,
			"size" => 75,
			"price" => "295.00",
			"img" => "imgs/bottles/ww/ww601030.gif",
			"desc" => "Premium quality blended scotch whisky from the Morrison Bowmore Distillers portfolio.",
			"paypal" => "",
			"instock" => 1,

			);
		$products[101009] = array(
		   "brand"=>"Ardbeg",
			"name" => "Lord of the Isles",
			"age" => "25 Year Old",
			"year" => "",
			"alcohol" => 46,
			"size" => 70,
			"price" => "1250.00",
			"img" => "imgs/bottles/ar/ar201025.gif",
			"desc" => "This whisky embodies the richness, depth and sweetness of all Ardbegs. Light gold in colour and powerful in character. A phenomenal, much-sought-after bottling of a really outstanding malt.",
			"paypal" => "",
			"instock" => 1,

			);
		$products[101010] = array(
			"brand"=>"Laphroaig",
			"name" => "HighGrove",
			"age" => "12 Year Old",
			"year" => "",
			"alcohol" => 46,
			"size" => 70,
			"price" => "195.00",
			"img" => "imgs/bottles/lr/lr401012.gif",
			"desc" => "A very special single cask Laphraoig, the official home of Prince Charles. The prince is a big fan of Laphroaig so we imagine he'll be very pleased with this. It's rather good.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[101011] = array(
			"brand"=>"Bunnahabhain",
			"name" => "Single Malt Scotch Whisky",
			"age" => "25 Year Old",
			"year" => "",
			"alcohol" => 43,
			"size" => 70,
			"price" => "275.00",
			"img" => "imgs/bottles/bh/bh501025.gif",
			"desc" => "A more sherried style, with the soft, creamy complexity and gentle, sophisticated palate and mouthfeel that Bunna's aficionados treasure. Handsomely presented, this is a delicious, contemplative dram. Picked up the top prize in its category at the World Whisky Awards 2010.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[101012] = array(
			"brand"=>"Laphroaig",
			"name" => "HighGrove",
			"age" => "12 Year Old",
			"year" => "",
			"alcohol" => 46,
			"size" => 70,
			"price" => "195.00",
			"img" => "imgs/bottles/lr/lr401012.gif",
			"desc" => "A very special single cask Laphraoig, the official home of Prince Charles. The prince is a big fan of Laphroaig so we imagine he'll be very pleased with this. It's rather good.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[101013] = array(
			"brand"=>"Bunnahabhain",
			"name" => "Single Malt Scotch Whisky",
			"age" => "25 Year Old",
			"year" => "",
			"alcohol" => 43,
			"size" => 70,
			"price" => "275.00",
			"img" => "imgs/bottles/bh/bh501025.gif",
			"desc" => "A more sherried style, with the soft, creamy complexity and gentle, sophisticated palate and mouthfeel that Bunna's aficionados treasure. Handsomely presented, this is a delicious, contemplative dram. Picked up the top prize in its category at the World Whisky Awards 2010.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[101014] = array(
			"brand"=>"Ardbeg",
			"name" => "Single Malt Scotch Whisky",
			"age" => "17 Year Old",
			"year" => "",
			"alcohol" => 40,
			"size" => 70,
			"price" => "395.00",
			"img" => "imgs/bottles/ar/ar202017.gif",
			"desc" => "Increasingly rare, now discontinued whisky from Ardbeg. Incredibly rich and powerful, and all too easy to drink.",
			"paypal" => "",
			"instock" => 1,

		);
		$products[101015] = array(
			"brand"=>"Test Bottle",
			"name" => "Test Bottle",
			"age" => "Just made",
			"year" => "distilled in 2013",
			"alcohol" => 15,
			"size" => 70,
			"price" => 00.01,
			"img" => "imgs/bottles/101001.gif",
			"desc" => "A test bottle for paypal account",
			"paypal" => "",
			"instock" => 1,
		);
		$products[101016] = array(
			"brand"=>"Wolf Legend",
			"name" => "Bowmore",
			"age" => "30 Year Old",
			"year" => "",
			"alcohol" => 43,
			"size" => 75,
			"price" => "295.00",
			"img" => "imgs/bottles/ww/ww601030.gif",
			"desc" => "Premium quality blended scotch whisky from the Morrison Bowmore Distillers portfolio.",
			"paypal" => "",
			"instock" => 1,

			);
		$products[101017] = array(
		   "brand"=>"Ardbeg",
			"name" => "Lord of the Isles",
			"age" => "25 Year Old",
			"year" => "",
			"alcohol" => 46,
			"size" => 70,
			"price" => "1250.00",
			"img" => "imgs/bottles/ar/ar201025.gif",
			"desc" => "This whisky embodies the richness, depth and sweetness of all Ardbegs. Light gold in colour and powerful in character. A phenomenal, much-sought-after bottling of a really outstanding malt.",
			"paypal" => "",
			"instock" => 1,

			);
		$products[101018] = array(
			"brand"=>"Laphroaig",
			"name" => "HighGrove",
			"age" => "12 Year Old",
			"year" => "",
			"alcohol" => 46,
			"size" => 70,
			"price" => "195.00",
			"img" => "imgs/bottles/lr/lr401012.gif",
			"desc" => "A very special single cask Laphraoig, the official home of Prince Charles. The prince is a big fan of Laphroaig so we imagine he'll be very pleased with this. It's rather good.",
			"paypal" => "",
			"instock" => 1,

		);

		foreach($products as $product_id => $product){
				$products[$product_id]["sku"] = $product_id;
		}

		return $products;
}




