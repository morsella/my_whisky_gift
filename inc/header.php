<?php if(!isset($meta)){
	$meta = "My Whisky Gift offers a Rare and exclusive Single Malt Scotch Whisky Collection which can be a perfect Gift for anyone who loves Whisky";
	}
?>

<!DOCTYPE html>
<html xml:lang="en" lang="en" class="no-js">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

  	<meta name="generator" content="Coda 1.7.4" />
	<meta name="robots" content="all" />
	<meta name="author" content="MorSella" />
	<meta name="keywords" content="Rare, Whisky, Collection" />
	<meta name="description" content="<?php echo $meta; ?>" />
	<?php if($selectedPage == "receipt"){echo '<meta http-equiv="refresh" content="10; URL=' .BASE_URL . '" />'; } ?>
	<?php if(isset($_GET["status"]) && $_GET ["status"] == "thanks"){ echo '<meta http-equiv="refresh" content="4; URL=' .BASE_URL . '" />'; } ?>



		<!-- _____________CSS LINKS___________________________________ -->
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/normalize.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/grid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/style.css" type="text/css" media="screen" />
<!-- 	<link rel="stylesheet" href="<?php echo BASE_URL; ?>vid/controlers.css" type="text/css" media="screen" /> -->

		<!-- _____________Java Script & Jquery_________________________-->
	<script type="text/javascript" src="<?php echo BASE_URL; ?>js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL; ?>js/imgmap.js"></script>
	<!-- VIDEO -->
	<script src="<?php echo BASE_URL; ?>vid/video.js" type="text/javascript" ></script>
<!-- 	<script src="<?php echo BASE_URL; ?>vid/my-controlers.js" type="text/javascript" language="javascript"></script> -->
	<!-- END VIDEO -->

	<script type="text/javascript" src="<?php echo BASE_URL; ?>js/lte-ie7.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.slider.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.touchSwipe.min.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL; ?>js/matchmedia.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL; ?>js/picturefill.js"></script>



	<title><?php echo $pageTitle; ?></title>
</head>
<body>
	<div class="pushMenu">

			<!-- Activated on mobile menu -->
			<!-- _____________HEADER with Interactive Navigation__________________________________ -->
		<header>

			<div id="logo"><a href="<?php echo BASE_URL; ?>" title="Return to Main page" class="svg">
					<object data="<?php echo BASE_URL; ?>imgs/logo.svg" type="image/svg+xml" class="pointer">
 		 		 				<!--[if lte IE8 ]-->
 		 		 				<img src="<?php echo BASE_URL; ?>imgs/logo.png" alt="My Whisky Gift Private Collection Logo"  />
 		 		 				<!--![endif]-->
 		 			</object>
 		 			</a>
			</div>
				<nav class="horisontalNav">
					<ul>
						<li class="<?php if($selectedPage == "home") {echo "active";} ?>"><a href="<?php echo BASE_URL; ?>" title="Return to Main page">Home</a></li>
						<li class="<?php if($selectedPage == "about") {echo "active";} ?>"><a href="<?php echo BASE_URL; ?>about/" title="Meet the Owner">About</a></li>
						<li class="<?php if($selectedPage == "blog") {echo "active";} ?>"><a href="<?php echo BASE_URL; ?>blog/" title="Read Articles and follow the my Whysky gift Blog">Blog</a></li>
						<li class="<?php if($selectedPage == "collection") {echo "active";} ?>"><a href="<?php echo BASE_URL; ?>collection/" title="My Whisky Gift Collection">Collection</a></li>
						<li class="<?php if($selectedPage == "contact") {echo "active";} ?>"><a href="<?php echo BASE_URL; ?>contact/" title="Contanct My Whisky Gift Team">Contact</a></li>
						<li><a target="paypal" href="https://www.paypal.com/cgi-bin/webscr?cmd=_cart&amp;business=EBEB5CRCSL5GQ&amp;display=1" title="My Whisky Gift Shopping Cart with PayPal"><div class="icon-cart svg_link <?php if($selectedPage == "cart") {echo " svg_active";} ?>"></div></a></li>
						<li><a href="<?php echo BASE_URL; ?>search/" title="Search for a Whisky Bottle"><div class="icon-magnifying-glass svg_link <?php if($selectedPage == "search") {echo " svg_active";} ?>" ></div></a></li>
					</ul>
				</nav>
				<button id="slideLeft" class="icon-menu"> MENU</button>
				<nav class="menu">
					<ul>
						<li class="<?php if($selectedPage == "home"){echo "active";} ?>"><a href="<?php echo BASE_URL; ?>" title="Return to Main page">Home</a></li>
						<li class="<?php if($selectedPage == "about") {echo "active";} ?>"><a href="<?php echo BASE_URL; ?>about/" title="Meet the Owner">About</a></li>
						<li class="<?php if($selectedPage == "blog") {echo "active";} ?>"><a href="<?php echo BASE_URL; ?>blog/" title="Read Articles and follow the my Whysky gift Blog">Blog</a></li>
						<li class="<?php if($selectedPage == "collection"){echo "active";} ?>"><a href="<?php echo BASE_URL; ?>collection/" title="My Whisky Gift Collection">Collection</a></li>
						<li class="<?php if($selectedPage == "contact"){echo "active";} ?>"><a href="<?php echo BASE_URL; ?>contact/" title="Contanct My Whisky Gift Team">Contact</a></li>
						<li><a target="paypal" href="https://www.paypal.com/cgi-bin/webscr?cmd=_cart&amp;business=EBEB5CRCSL5GQ&amp;display=1" title="My Whisky Gift Shopping Cart with PayPal"><div class="icon-cart svg_link <?php if($selectedPage == "cart") {echo "active";} ?>"></div></a></li>
						<li><a href="<?php echo BASE_URL; ?>search/" title="Search for a Whisky Bottle"><div class="icon-magnifying-glass svg_link <?php if($selectedPage == "search") {echo "active";} ?>" ></div></a></li>
					</ul>
				</nav>
				<div class="clear"></div>
		</header>
