		<div class="grid_12 sort_border">
			<div class="grid_6 sort_border1 alpha">
				<img src="<?php echo BASE_URL; ?>imgs/image_map_01.gif" alt="My Whisky Gift Sorting Options" />
			</div>
			<div class="grid_6 sort_border2 omega">
				<img src="<?php echo BASE_URL; ?>imgs/image_map_02.gif" alt="My Whisky Gift Sorting Options" />
			</div>
			<ul class="grid_12 sort">
					<li class="macallan grid_2"><img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/the_macallan.gif" alt="Sort Whisky by Brand Macallan" />
					<span class="hover"><a class="mouse-events" href="<?php echo BASE_URL; ?>collection/macallan/"><img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/the_macallan_o.gif" alt="Sort Whisky by Brand Macallan" /></a></span> 
					</li> 
					<li class="ardbeg grid_2"><img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/ardbeg.gif" alt="Sort Whisky by Brand Ardbeg" />
					<span class="hover"><a class="mouse-events" href="<?php echo BASE_URL; ?>collection/ardbeg/"><img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/ardbeg_o.gif" alt="Sort Whisky by Brand Ardbeg" /></a></span> 
					</li> 
					<li class="bowmore grid_2"><img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/bowmore.gif" alt="Sort Whisky by Brand Bowmore" />
					<span class="hover"><a class="mouse-events" href="<?php echo BASE_URL; ?>collection/bowmore/"><img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/bowmore_o.gif" alt="Sort Whisky by Brand Bowmore" /></a></span> 
					</li> 
					<li class="view_all grid_2"><img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/view_all.gif" alt="Sort Whisky by viewing all the collection" />
					<span class="hover"><a class="mouse-events" href="<?php echo BASE_URL; ?>collection/view-all/"><img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/view_all_o.gif" alt="View All the Collection" /></a></span> 
					</li> 
			</ul>
			<div class="grid_12 image_map" >
				<img src="<?php echo BASE_URL; ?>imgs/image_map.gif" alt="Sort Whisky by viewing all the collection" id="image_map" usemap="#imgMap" />
			</div>
		</div>	
			 
			 <map class="grid_12" name="imgMap" id="imgMap">
			 <area shape="circle" coords="174,149,52" href="<?php echo BASE_URL; ?>collection/macallan/" alt="blue" onmouseover="swapImage('image_map', 'image_map_m.gif' ); changeTitle(1);" onmouseout="swapImage('image_map','image_map.gif'); changeTitle(0);" />
              <area shape="circle" coords="340,148,52"  href="<?php echo BASE_URL; ?>collection/ardbeg/" alt="purple" onmouseover="swapImage('image_map', 'image_map_a.gif' ); changeTitle(2);" onmouseout="swapImage('image_map','image_map.gif'); changeTitle(0);" />            
              <area shape="circle" coords="503,151,53"  href="<?php echo BASE_URL; ?>collection/bowmore/" alt="pink" onmouseover="swapImage('image_map', 'image_map_b.gif' ); changeTitle(3);" onmouseout="swapImage('image_map','image_map.gif'); changeTitle(0);" />
              <area shape="circle" coords="669,148,52"  href="<?php echo BASE_URL; ?>collection/view-all/" alt="yellow" onmouseover="swapImage('image_map', 'image_map_v.gif' ); changeTitle(4);" onmouseout="swapImage('image_map','image_map.gif'); changeTitle(0);" />
			 </map>
			 <div class="grid_12" id="subtitle">
			  <h4>Choose Whisky by Brand or <br />View All the bottles in the Collection</h4><br />
			</div>

