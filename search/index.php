<?php 

require_once("../inc/config.php");

$search_term = "";
if(isset($_GET["search"])){
	$search_term = trim($_GET["search"]);
	if($search_term != ""){
		require_once(ROOT_PATH."inc/products.php");
		$products = get_products_search($search_term);
	}
}

$pageTitle = "My Whisky Gift &#124; Search Whisky";
$selectedPage = "search";
include(ROOT_PATH. 'inc/header.php'); ?>
	<section class="container">
		<div class="grid_12 search">
			<img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/devider_1.gif" alt="Whisky bottles devider" />
			<h1>Search for a specific Whisky Bottle</h1>
			<h4>Type in the name or the brand. <br />You can also search by age or by year</h4><br />
			<form method="get" action="./">
				<label class="icon-magnifying-glass svg_link"></label>
				<input type="text" size="25" name="search" value="<?php if (isset($search_term)){echo htmlspecialchars($search_term);} ?>" />
				<input type="submit" value="Go" id="submit" />
			</form>
			<?php
				if($search_term != ""){
					if(!empty($products)){
						echo '<ul class="collection">';
						 foreach($products as $product){
						   echo get_list_view_html($product);
						  }
						echo '</ul>';
						}else{
							echo '<p>No results were found matching that search term. </p><br />';
						}
				}
			?>
		</div><br />
	</section>	
<?php include(ROOT_PATH . "inc/footer.php"); ?>