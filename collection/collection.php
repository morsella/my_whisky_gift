<?php 
	require_once("../inc/config.php");
	require_once(ROOT_PATH . "inc/products.php");
	
	// retrieve current page number from query string; set to 1 if blank
	if(empty($_GET["pg"])){
			$current_page = 1;
	}else{
		$current_page = $_GET["pg"];
	}
	
	// set strings like "foo" to 0; remove decimals
	$current_page = intval($current_page);
	
	$total_products = get_products_count();
	$products_per_page = 9;
	$total_pages = ceil($total_products / $products_per_page);
	
	// redirect too large page numbers to the last page
	if($current_page > $total_pages){
		header("Location: ./?pg=" . $total_pages);
	}
	
	// redirect 0 page number to the first page
	if($current_page <1){
		header("Location: ./");
	}
	
	// determine the start and the end product for the current page;
	// for example on page 2 with 9 bottles per page
	// $start and $end would be 10 and 18
	$start = (($current_page -1) * $products_per_page) + 1;
	$end = $current_page * $products_per_page;
	if($end > $total_products){
		$end = $total_products;
	}
	
	$products = get_products_subset($start, $end);
	
?><?php 
$pageTitle = "My Whisky Gift &#124; Exclusive Collection";
$selectedPage = "collection";
include(ROOT_PATH. 'inc/header.php') ?>
	<section class="container">
	<h1 id="collection_title">My Whisky Gift Collection</h1>
	<?php include(ROOT_PATH.'inc/partial_image_map.html.php') ?>					
		<div class="grid_12 productsList">
					<img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/devider_1.gif" alt="Whisky bottles devider" />
			<?php include(ROOT_PATH."inc/partial-list-nav.php"); ?>
			<div class="grid_12">
				<ul class="collection">
						<?php foreach($products as $product) { 
								echo get_list_view_html($product);
							}
						?>
				</ul>
			</div>
		</div>
		<?php include(ROOT_PATH."inc/partial-list-nav.php"); ?>
		<br />
	</section>

	<?php include(ROOT_PATH.'inc/partial_image_map_js.php'); ?>

	<?php include(ROOT_PATH.'inc/footer.php') ?>