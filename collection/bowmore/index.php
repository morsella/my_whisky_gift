<?php 
	require_once("../../inc/config.php");
	require_once(ROOT_PATH . "inc/products.php");
	$products = get_products_all();
?><?php 
$pageTitle = "My Whisky Gift &#124; Bowmore Collection";
$selectedPage = "collection";
$meta = "My Whisky Gift Bowmore Collection shows all the Whiskies in the Collection that were distilled by Bowmore";
include(ROOT_PATH. 'inc/header.php') ?>
	<section class="container">
		<div class="breadcrumb"> 
			<a href="<?php echo BASE_URL; ?>collection/">Collection</a> &gt; Bowmore
<!-- 				<img src="<?php echo BASE_URL; ?>imgs/devider.gif" alt="Whisky bottles devider" /> -->
 		</div>
		<div class="grid_12 productsList">
			<h1>My Whisky Gift - Bowmore Collection</h1>
			<h4>Displaying All Bowmore Whisky Bottles in the Collection</h4><br />
					<img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/devider_1.gif" alt="Whisky bottles devider" />
			<div class="grid_12">
				<ul class="collection">
						<?php foreach($products as $product) { 
							if($product["brand"] == "Bowmore"){
								echo get_list_view_html($product);
								}
							}
						?>
				</ul>
			</div>
		</div>
		<br />
	</section>
<?php include(ROOT_PATH. 'inc/footer.php') ?>