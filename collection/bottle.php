<?php 
	require_once("../inc/config.php");
	require_once(ROOT_PATH."inc/products.php"); 
	$products = get_products_all();

	if(isset($_GET["id"])){
		$product_id = $_GET["id"];
		if (isset($products[$product_id])){
			$product = $products[$product_id];
		}
	}
	if(!isset($product)){
		header("Location:" . BASE_URL . "collection/");
		exit();
	}
	
	$selectedPage = "";
	$pageTitle = $product["name"];
	include(ROOT_PATH."inc/header.php");
?><section class="container">
		<div class="grid_12">
			<div class="breadcrumb"> 
				<a href="<?php echo BASE_URL; ?>collection/">Collection</a> &gt; <?php echo $product["brand"] . " - " . $product["name"]; ?>
				<img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/devider.gif" alt="Whisky bottles devider" />
 			</div> 			
 			<div class="grid_6 bottleDescription">
				<h2><?php echo $product["brand"] . "<br /> " . $product["name"]; ?></h2><br />
				<h4><?php echo $product["age"] . " " . $product["year"]; ?>
					<br /> &#40; <?php echo $product["size"] . 'cl, '. $product["alcohol"]; ?>&#37; &#41;
				</h4><br />
				<p><?php echo $product["desc"]; ?></p><br />
				<div class="grid_3 bottlePrice">
					<h5>&#8364;<?php echo $product["price"]; ?></h5>
					<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					<input type="hidden" name="cmd" value="_s-xclick" />
					<input type="hidden" name="hosted_button_id" value="<?php echo $product["paypal"]; ?>" />
					<input type="hidden" name="item_name" value="<?php echo $product["brand"]. " " . $product["name"] . " " . $product["age"]. " " . $product["year"]; ?>" />
					<input type="submit" value="Add to Cart" name="submit" id="toCart"/>
					</form>
				</div>
 			</div>
			<div class="grid_6 bottleImage">
				<div id="productBottle"><img class="pointer-events" src="<?php echo BASE_URL.$product["img"]; ?>"  alt="<?php echo $product["brand"]. " " . $product["name"]; ?>" /></div>
				<div id="productBottle2"><img class="pointer-events" src="<?php echo BASE_URL.$product["img2"]; ?>"  alt="<?php echo $product["brand"]. " " . $product["name"]; ?>" /></div>
				<div id="productBottle3"><img class="pointer-events" src="<?php echo BASE_URL.$product["img3"]; ?>"  alt="<?php echo $product["brand"]. " " . $product["name"]; ?>" /></div>
				<div id="productBottle4"><img class="pointer-events" src="<?php echo BASE_URL.$product["img4"]; ?>"  alt="<?php echo $product["brand"]. " " . $product["name"]; ?>" /></div>
				<ul class="thumbnails">	
					<li class="grid_3"><button id="tmb1" class="active_tmb"><img class="pointer-events" src="<?php echo BASE_URL.$product["tmb1"]; ?>" alt="<?php echo $product["brand"]. " " . $product["name"]; ?>" /></button></li>
					<li class="grid_3"><button id="tmb2"><img class="pointer-events" src="<?php echo BASE_URL.$product["tmb2"]; ?>" alt="<?php echo $product["brand"]. " " . $product["name"]; ?>" /></button></li>
					<li class="grid_3"><button id="tmb3"><img class="pointer-events" src="<?php echo BASE_URL.$product["tmb3"]; ?>" alt="<?php echo $product["brand"]. " " . $product["name"]; ?>" /></button></li>
					<li class="grid_3"><button id="tmb4"><img class="pointer-events" src="<?php echo BASE_URL.$product["tmb4"]; ?>" alt="<?php echo $product["brand"]. " " . $product["name"]; ?>" /></button></li>
				</ul>
			</div>
		</div>	
	</section>
<?php include(ROOT_PATH.'inc/partial_product_thumbs.html.php'); ?>
<?php include(ROOT_PATH."inc/footer.php"); ?>
	