<?php 
	require_once("../../inc/config.php");
	require_once(ROOT_PATH . "inc/products.php");
	$products = get_products_all();
?><?php 
$pageTitle = "My Whisky Gift &#124; Exclusive Collection &#124; All the Brands ";
$selectedPage = "collection";
include(ROOT_PATH. 'inc/header.php') ?>
	<section class="container">
		<div class="breadcrumb"> 
			<a href="<?php echo BASE_URL; ?>collection/">Collection</a> &gt; All 
<!-- 				<img src="<?php echo BASE_URL; ?>imgs/devider.gif" alt="Whisky bottles devider" /> -->
 		</div>
		<div class="grid_12 productsList">
			<h1>My Whisky Gift Collection</h1>
			<h4>Displaying All the Whisky Bottles in the Collection</h4><br />
					<img class="pointer-events" src="<?php echo BASE_URL; ?>imgs/devider_1.gif" alt="Whisky bottles devider" />
			<div class="grid_12">
				<ul class="collection">
						<?php foreach($products as $product) { 
								echo get_list_view_html($product);
							}
						?>
				</ul>
			</div>
		</div>
		<br />
	</section>
<?php include(ROOT_PATH. 'inc/footer.php') ?>