﻿<?php 
	
	require_once("../inc/config.php");

$pageTitle = "My Whisky Gift &#124; Flash Content";
$selectedPage = "flash";
$meta = "My Whisky Gift Flash Content include various Whisky commercials and Scottish singles";
include(ROOT_PATH."inc/header.php"); 
include(ROOT_PATH."inc/products.php");
?>
<section class="container">
	<div class="grid_12 flashPage">
	<h1>My Whisky Gift Flash Content</h1>
		<div>
			<object width="1024" height="768" data="preloadersplash.swf" type="application/x-shockwave-flash">
				<param name="movie" value="preloadersplash.swf" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#f9f9f9" />
				<param name="play" value="true" />
				<param name="loop" value="true" />
				<param name="wmode" value="window" />
				<param name="scale" value="showall" />
				<param name="menu" value="true" />
				<param name="devicefont" value="false" />
				<param name="salign" value="" />
				<param name="allowScriptAccess" value="sameDomain" />
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="preloadersplash.swf" width="1024" height="768">
					<param name="movie" value="preloadersplash.swf" />
					<param name="quality" value="high" />
					<param name="bgcolor" value="#f9f9f9" />
					<param name="play" value="true" />
					<param name="loop" value="true" />
					<param name="wmode" value="window" />
					<param name="scale" value="showall" />
					<param name="menu" value="true" />
					<param name="devicefont" value="false" />
					<param name="salign" value="" />
					<param name="allowScriptAccess" value="sameDomain" />
				<!--<![endif]-->
					<a href="http://www.adobe.com/go/getflash">
						<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
					</a>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
		</div>

	</div>
</section>
<?php include(ROOT_PATH."inc/footer.php"); ?>	
